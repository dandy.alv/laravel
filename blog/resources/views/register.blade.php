<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label for="firstname">First name:</label>
        <br><br>
        <input type="text" id="firstname" name="firstname">
        <br><br>
        <label for="lastname">Last name:</label>
        <br><br>
        <input type="text" id="lastname" name="lastname">
        <br>
        <p>Gender:</p>
        <input type="radio" id="male" name="gender" value="male"><label for="male">Male</label>
        <br>
        <input type="radio" id="female" name="gender" value="female"><label for="female">Female</label>
        <br>
        <input type="radio" id="other_gender" name="gender" value="other_gender"><label for="other_gender">Other</label>
        <br><br>
        <label for="nationality">Nationality:</label>
        <br><br>
        <select name="nationality" id="nationality">
            <option value="1">Indonesian</option>
            <option value="2">English</option>
            <option value="3">French</option>
            <option value="4">Chinese</option>
            <option value="5">Other</option>
        </select>
        <br>
        <p>Language Spoken:</p>
        <input type="checkbox" id="bahasa" name="language" value="bahasa"><label for="bahasa">Bahasa Indonesia</label>
        <br>
        <input type="checkbox" id="english" name="language" value="english"><label for="english">English</label>
        <br>
        <input type="checkbox" id="other_language" name="language" value="other_language"><label for="other_language">Other</label>
        <br><br>
        <label for="bio">Bio:</label>
        <br><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
        <br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>